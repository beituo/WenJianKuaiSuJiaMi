# 文件快速加密
用Java实现的文件快速加密。其加密方式非常简单，和Python版的文件简单加密（https://gitee.com/CaoGuZi/WenJianJianDanJiaMi）一样。但是，Java实现采用了多线程、NIO以及内存文件映射，其速度要远远超过Python版实现。
实测结果：加密大小为1G的文件，Java版耗时为3秒左右，而Python版则需要8分钟左右，而且还可能会出现内存溢出错误。
Java版本：1.8
平台：Win7
加密命令：java -jar encode.jar encode filename [key]
解密命令：java -jar encode.jar decode filename [key]
默认key值：TESTKEY
注：encode.jar是将此项目打成的jar包。